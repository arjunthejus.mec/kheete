from fastapi import FastAPI, Request, HTTPException, Depends, Cookie
from fastapi.responses import HTMLResponse, RedirectResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from starlette import status
from psycopg2.extensions import connection
from typing import Optional
from paho.mqtt.client import Client
from datetime import datetime

from khetee.essentials.db import DatabaseConnector
from khetee.essentials.mqtt import MqttConnector
from khetee.essentials.auth import NewUser, User, Token
from khetee.essentials.ml import Model

from threading import Timer
import json
import logging
from typing import Dict

app = FastAPI()
running_timers = {}  # Contains all the running timers.
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

database_connector = DatabaseConnector()
mqtt_connector = MqttConnector()
token = Token()
# model = Model()  # TODO Model has to be different for different people.

# Setting up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(levelname)s:%(name)s:%(message)s")

# Handler to write into app.log
file_handler = logging.FileHandler("logs/app.log")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.ERROR)  # app.log will log error and above

# Handler to stream into console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)  # logger will stream debug and above

logger.addHandler(file_handler)
logger.addHandler(stream_handler)


@app.post("/api/v1/record")
async def record(request: Request):
    body = await request.body()
    values = body.split()
    for value in values:
        value = str(value)
        logger.debug(value.split(":")[1][:-1])

    return status.HTTP_200_OK


@app.get("/", response_class=HTMLResponse)
async def login_page(request: Request, authenticated: Dict = Depends(token)):
    """'Dict' authenticated will be {
                "valid": True,
                "name": name,
                "username": email,
                "userid": userid,
                "mac": mac,
            }
    if the request contains a vaild token. If the token is invalid/expired,
    it will be {"valid": False, "name": "Login/Register"}

    This function renders the second element of class :str: authenticated into the template.
    """
    status = authenticated["name"]
    return templates.TemplateResponse(
        "home.html", {"request": request, "status": status}
    )


@app.get("/login", response_class=HTMLResponse)
async def login_page(request: Request, authenticated: Dict = Depends(token)):
    """'Dict' authenticated will be [bool: True, str: userid/email] if the request contains
    a vaild token. If the token is invalid/expired, it will be [bool: False, "Login/Register"]

    If the token is valid, i.e authenticated["valid"] == True;
    The user will be redirected to '/profile'.

    Otherwise, he will receive the login page.
    """
    if authenticated["valid"]:
        return RedirectResponse("/profile")
        # return templates.TemplateResponse("test.html", {"request": request})
    else:
        return templates.TemplateResponse("index.html", {"request": request})


@app.get("/forgotpassword", response_class=HTMLResponse)
async def forgot_password(request: Request, authenticated: Dict = Depends(token)):
    """'Dict' authenticated will be {
                "valid": True,
                "name": name,
                "username": email,
                "userid": userid,
                "mac": mac,
            }
    if the request contains a vaild token. If the token is invalid/expired,
    it will be {"valid": False, "name": "Login/Register"}


    If the token is valid, i.e authenticated["valid"] == True;
    The user will be redirected to '/profile'.

    Otherwise, he will receive the password reset page.
    """
    if authenticated["valid"]:
        return RedirectResponse("/profile")
        # return templates.TemplateResponse("test.html", {"request": request})
    else:
        return templates.TemplateResponse("forgotpwd.html", {"request": request})


@app.get("/register", response_class=HTMLResponse)
async def signup_page(request: Request, authenticated: Dict = Depends(token)):
    """'Dict' authenticated will be {
                "valid": True,
                "name": name,
                "username": email,
                "userid": userid,
                "mac": mac,
            }
    if the request contains a vaild token. If the token is invalid/expired,
    it will be {"valid": False, "name": "Login/Register"}

    If the token is valid, i.e authenticated["valid"] == True;
    The user will be redirected to '/profile'.

    Otherwise, he will receive the registration page.
    """
    if authenticated["valid"]:
        return RedirectResponse("/profile")
        # return templates.TemplateResponse("test.html", {"request": request})
    else:
        return templates.TemplateResponse("reg.html", {"request": request})


@app.get("/profile", response_class=HTMLResponse)
async def profile_page(request: Request, authenticated: Dict = Depends(token)):
    """'Dict' authenticated will be {
                "valid": True,
                "name": name,
                "username": email,
                "userid": userid,
                "mac": mac,
            }
    if the request contains a vaild token. If the token is invalid/expired,
    it will be {"valid": False, "name": "Login/Register"}

    If the token is valid, i.e authenticated["valid"] == True;
    The user will be directed to his profile.

    Otherwise, he will be redirected to '/login'.
    """
    if authenticated["valid"]:
        return templates.TemplateResponse(
            "dashboard.html",
            {
                "request": request,
                "name": authenticated["name"],
                "email": authenticated["username"],
                "mac": authenticated["mac"],
            },
        )
    else:
        return RedirectResponse("/login")


@app.post("/login/submit")
async def login(request: Request, connector: connection = Depends(database_connector)):
    """Credentials are accepted as POST request. connector is of :class: connection returned
    by database_conncector which is an instance of :class: DatabaseConnector.

    If the received 'username' and 'password' matches an account, an authentication token
    will be generated and returned to the user as http only cache.

    Otherwise, 401 will be returned to indicate error in credentials.
    """
    try:
        user = await request.json()
        # View documentation of 'auth.py' to know more about class User
        # user is an instance of :class: 'dict'.
        # User(**user) will initialize the variables in the new 'User' instance having
        # same name as keys of 'dict' user with their corresponding values.
        user = User(**user)
        user_verified = await user.verify_credentials(connector)

        if user_verified:
            # Get username and user_id to generate token
            cursor = connector.cursor()
            query = "SELECT name, user_id FROM user_credentials WHERE username = %s"
            query = cursor.mogrify(query, (user.username,))
            cursor.execute(query)
            name, userid = cursor.fetchone()
            # On successful user verification, token is generated
            new_token = token.create_access_token(
                {"userid": userid, "name": name, "username": user.username}
            )
            response = JSONResponse(content={"detail": "success"})
            # Token is embedded as httponly cookie to prevent xss
            response.set_cookie(key="k_auth", value=new_token, httponly=True)
            return response

        else:
            return HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
            )

    except Exception as exp:
        logger.exception("Critical: ")


@app.post("/profile/graph")
async def get_graph(
    request: Request,
    authenticated: Dict = Depends(token),
    connector: connection = Depends(database_connector),
):
    """Credentials are accepted as POST request. connector is of :class: connection returned
    by database_conncector which is an instance of :class: DatabaseConnector. token of class
    :Token: verifies authenticaton key is genuine.

    Returns the duration of irrigation by date.
    """
    if authenticated["valid"]:
        cursor = connector.cursor()
        # Fetch data
        query = "SELECT date, duration FROM {} ORDER BY date"
        cursor.execute(query.format(authenticated["mac"]))
        data = cursor.fetchall()
        # Process fetch data
        ret_labels, ret_data = zip(*data)
        ret_labels = list(map(lambda x: x.date(), ret_labels))

        return (ret_data, ret_labels)


@app.get("/logout")
async def route_logout_and_remove_cookie():
    """Deletes authentication key"""
    response = RedirectResponse("/")
    response.delete_cookie("k_auth")
    return response


@app.post("/register/submit")
async def register(
    new_user: NewUser, connector: connection = Depends(database_connector)
):
    """User details are accepted as POST request. connector is of :class: connection returned
    by database_conncector which is an instance of :class: DatabaseConnector.
    """
    logger.debug("User Reg")
    try:
        cursor = connector.cursor()
        # Insert name, username and hashed password into the database.
        query = "INSERT INTO user_credentials (name, username, password) VALUES (%s, %s, %s) RETURNING user_id"
        query = cursor.mogrify(
            query, (new_user.name, new_user.email, new_user.password1)
        )
        cursor.execute(query)
        user_id = cursor.fetchone()[0]

        # Register mac to the user.
        query = "UPDATE mac_list SET user_id = %s WHERE mac = %s"
        query = cursor.mogrify(query, (user_id, new_user.mac))
        cursor.execute(query)

        # Create a table to track the usage of the user.
        query = """CREATE TABLE {0}(
            "id" integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
            "date" timestamptz NOT NULL,
            "duration" float
            );"""
        logger.debug(query.format(new_user.mac))
        cursor.execute(query.format(new_user.mac))

        # Commit all changes.
        connector.commit()

    except:
        # In case of an error, roll back all the changes.
        connector.rollback()
        logger.exception("Registration Error!")


def stop_session(key, mac):
    # Function to stop a session.
    logger.debug("stopping session")
    client = mqtt_connector()
    # Message is publiced on topic khetee/action/<mac>
    client.publish("khetee/action/" + mac, "stop", qos=1)
    logger.debug(running_timers.keys())
    try:
        # Stop the timer. Incase stop is called by the user and not the timer.
        running_timers[key].cancel()
        del running_timers[key]

    except KeyError:
        pass

    except:
        logger.exception("Error:")
    logger.debug(running_timers.keys())


@app.post("/start")
async def start(
    request: Request,
    authenticated: Dict = Depends(token),
    client: Client = Depends(mqtt_connector),
    connector: connection = Depends(database_connector),
):
    """token of :class: Token will authenticate the authentication token.
    client of :class: Client establishes an MQTT connection.

    :message: 'start' is published to the mac corresponding to the authentication token
    A timer is run on a thread to stop after user specified/default time.
    """

    logger.debug("Called: Start")
    cursor = connector.cursor()
    if not authenticated["valid"]:
        return HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )

    else:
        # Request will contain time of irrigation.
        request = await request.json()
        # Published to khetee/action/<mac> to start.
        client.publish("khetee/action/" + authenticated["mac"], "start", qos=1)
        duration = request["time"]
        if duration:
            h, m, s = duration.split(":")
            h, m, s = int(h), int(m), int(s)
            duration_in_seconds = h * 3600 + m * 60 + s

        else:
            # If duration is not specified the timer will run for only 5 seconds.
            duration_in_seconds = 5

        # Initialising timer.
        timer = Timer(
            duration_in_seconds,
            stop_session,
            [authenticated["userid"], authenticated["mac"]],
        )
        # Insert the timer into global timers
        running_timers[
            authenticated["userid"]
        ] = timer  # authenticated["userid"] := userid
        # Run the timer
        running_timers[authenticated["userid"]].start()
        # TODO Insert into database the start time and duration if specified
        # This code is only to demonstrate
        query = "INSERT INTO {} (date, duration) VALUES (%s, %s)".format(
            authenticated["mac"]
        )
        query = cursor.mogrify(
            query,
            (datetime.now(), round(duration_in_seconds / 60, 1)),
        )
        cursor.execute(query)
        try:
            connector.commit()
        except:
            logger.exception("Error!")
            connector.rollback()


@app.post("/stop")
async def stop(request: Request, authenticated: Dict = Depends(token)):
    """token of :class: Token verifies the authentication token is genuine and valid.
    :function: stop is called to stop the device
    """
    if not authenticated["valid"]:
        return HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )

    else:
        stop_session(authenticated["userid"], authenticated["mac"])
        # TODO Insert into database the stop time.


@app.post("/predict")
async def predict(
    request: Request,
    authenticated: Dict = Depends(token),
    connector: connection = Depends(database_connector),
):
    """token of :class: Token verifies the authentication token is genuine and valid.

    This function reads the last 10 watering duration of user from database, pass it
    to the machine learning model for prediction, return the time as a JSON response.
    """
    if not authenticated["valid"]:
        return HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )
    try:
        model = Model(authenticated["mac"])
        cursor = connector.cursor()
        # Read last 10 readings.
        query = "SELECT duration FROM {} ORDER BY date desc limit 10"
        cursor.execute(query.format(authenticated["mac"]))
        data = cursor.fetchall()
        # Process the read data.
        data = list(map(lambda x: x[0], data))
        data.reverse()
        logger.debug(data)

        # Get the prediction.
        prediction = model.predict(data)
        return JSONResponse(content={"prediction": prediction})

    except ValueError:
        return JSONResponse(content={"prediction": "00:00:00"})

    except:
        logger.critical("Error!")
