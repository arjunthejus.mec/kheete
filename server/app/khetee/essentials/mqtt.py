import paho.mqtt.client as paho
from time import sleep
import os

from dotenv import load_dotenv
import logging

# Setting up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(levelname)s:%(name)s:%(message)s")

# Handler to write into app.log
file_handler = logging.FileHandler("logs/mqtt.log")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.ERROR)  # db.log will log error and above

# Handler to stream into console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)  # logger will stream debug and above

logger.addHandler(file_handler)
logger.addHandler(stream_handler)


class MqttConnector:
    def _on_mqtt_connect(self, client, userdata, flags, rc):
        if rc == 0:
            logger.info("MQTT connection successful")
        else:
            logger.critical(f"MQTT connection failed with result code {str(rc)}")

    def __init__(self):
        load_dotenv()
        try:
            self.broker = os.getenv("MQTT_BROKER")
            self.port = int(os.getenv("MQTT_PORT"))
            self.user = os.getenv("MQTT_USER")
            self.password = os.getenv("MQTT_PASSWORD")

        except:
            logger.exception("MQTT initialisation failed!")

    def __call__(self):
        client = paho.Client("client")  # create client object
        client.on_connect = self._on_mqtt_connect
        # if self.password:
        # client.username_pw_set(username=self.user, password=self.password)
        # logger.info("got password")
        client.loop_start()
        client.connect(self.broker, self.port)
        sleep(1)
        client.loop_stop()
        return client
