"""This module is used to connect to the database.
"""
import time
import os
import psycopg2
import psycopg2.extras

from dotenv import load_dotenv
import logging

# Setting up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(levelname)s:%(name)s:%(message)s")

# Handler to write into app.log
file_handler = logging.FileHandler("logs/db.log")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.ERROR)  # db.log will log error and above

# Handler to stream into console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)  # logger will stream debug and above

logger.addHandler(file_handler)
logger.addHandler(stream_handler)


class DatabaseConnector:
    """The DatabaseConnector object establishes a connection with the
    database server. The get_connection method returns the connection.

    You can create a :class: 'DatabaseConnector' instance in you main
    module or any package like this::

        from db import DatabaseConnector
        database_connector = DatabaseConnector()

    You can initialize a cursor like this::

        cursor = database_connector().cursor()

    To establish a connection, credentials are to be provided as a .env file.
    The contents of the file should be like this::

    POSTGRES_HOST=xxx.xxx.xxx.xxx
    POSTGRES_PORT=xxxx
    POSTGRES_NAME=xxxxxx
    POSTGRES_USER=xxxxxx
    # POSTGRES_PASSWORD=xxxxxxxxxxxxx
    # POSTGRES_PASSWORD_FILE=xxxxxxx.txt

    Note to use POSTGRES_PASSWORD if password is an environment variable
    and POSTGRES_PASSWORD_FILE if password is to be read from a file.

    Alternatively you can provide them in the command line
    """

    conn = None

    def __init__(self):
        # Initializes the connection after reading values from the environment

        load_dotenv()  # Take environment variables from .env.

        if os.getenv("POSTGRES_PASSWORD"):
            postgres_password = os.getenv("POSTGRES_PASSWORD")

        elif os.getenv("POSTGRES_PASSWORD_FILE"):
            with open(os.getenv("POSTGRES_PASSWORD_FILE")) as f:
                postgres_password = f.read().strip()

        while True:
            try:
                logger.info("Trying to connect to the postgres database")
                self.conn = psycopg2.connect(
                    host=os.getenv("POSTGRES_HOST"),
                    port=os.getenv("POSTGRES_PORT"),
                    dbname=os.getenv("POSTGRES_NAME"),
                    user=os.getenv("POSTGRES_USER"),
                    password=postgres_password,
                )
                # Needed so that we can insert uuids
                psycopg2.extras.register_uuid()
                logger.info("Connection to database successful")
                break

            except psycopg2.OperationalError as e:
                logger.exception(
                    "Unable to connect to the database. Waiting for 5 seconds and retrying..."
                )
                time.sleep(5)
                continue

    def __call__(self):
        # For database_connector being an instance of :class: DatabaseConnector,
        # method __call__ can be called as database_connector()
        return self.conn
