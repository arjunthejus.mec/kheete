from threading import current_thread
from fastapi import Request, HTTPException, Cookie

from jose import jwt, exceptions
from starlette import status

from pydantic import BaseModel, validator
from typing import Optional
from datetime import datetime, timedelta
from passlib.context import CryptContext
from psycopg2.extensions import connection

from khetee.essentials.db import DatabaseConnector

import logging
from dotenv import load_dotenv
from os import getenv

load_dotenv()

SECRET_KEY = getenv("SECRET_KEY")
ALGORITHM = getenv("ALGORITHM")
ACCESS_TOKEN_EXPIRE_MINUTES = 20


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    "%(levelname)s:%(asctime)s:%(name)s:%(funcName)s:%(message)s"
)

# Handler to stream into console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)  # logger will stream debug and above

logger.addHandler(stream_handler)

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

database_connector = DatabaseConnector()


class User(BaseModel):
    username: str
    password: str

    async def verify_credentials(self, connector: connection):
        try:
            cursor = connector.cursor()
            query = "SELECT password FROM user_credentials WHERE username = %s"
            query = cursor.mogrify(query, (self.username,))
            cursor.execute(query)
            hashed_password = cursor.fetchone()[0]

            return pwd_context.verify(self.password, hashed_password)

        except:
            return False


class NewUser(BaseModel):
    name: str
    email: str
    mac: str
    password1: str
    password2: str

    @validator("password2")
    def passwords_match(cls, v, values, **kwargs):
        if "password1" in values and v != values["password1"]:
            raise ValueError("passwords do not match")

        else:
            v = values["password1"] = pwd_context.hash(v)

        return v

    @validator("email")
    def email_check(cls, v):

        connector = database_connector()
        cursor = connector.cursor()
        query = "SELECT user_id FROM user_credentials WHERE username = %s"
        query = cursor.mogrify(query, (v,))
        cursor.execute(query)
        id = cursor.fetchone()

        if not id:
            logger.debug("success")
            return v
        else:
            logger.debug("username already exists")
            raise ValueError("Email Id already exists")

        """try:
            name, domain = v.split("@")
            logger.info(name, domain)
            provider, extension = domain.split(".")
        except Exception as e:
            raise ValueError("Invalid email id")
        """

    @validator("mac")
    def mac_check(cls, v):
        connector = database_connector()
        cursor = connector.cursor()
        query = "SELECT id FROM mac_list WHERE mac = %s"
        query = cursor.mogrify(query, (v,))
        cursor.execute(query)
        id = cursor.fetchone()

        if id:
            logger.debug("success")
            return v
        else:
            logger.debug("mac id does not exist")
            raise ValueError("Mac Id already exists or is claimed")


class Token:
    def create_access_token(
        self, data: dict, expires_delta: Optional[timedelta] = None
    ):
        to_encode = data.copy()

        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)

        to_encode.update({"exp": expire})
        return jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    def verify_access_token(self, token: Optional[str] = None):
        try:
            payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])

            connector = database_connector()
            cursor = connector.cursor()
            query = "SELECT mac FROM mac_list WHERE user_id = %s"
            query = cursor.mogrify(query, (payload["userid"],))
            cursor.execute(query)
            mac = cursor.fetchone()[0]

            return {
                "valid": True,
                "name": payload["name"],
                "username": payload["username"],
                "userid": payload["userid"],
                "mac": mac,
            }

        except exceptions.JWTError:
            # Wrong/ Expired jwt failed authentication
            return {"valid": False, "name": "Login/Register"}

        except AttributeError:
            # No jwt
            return {"valid": False, "name": "Login/Register"}

    def __call__(self, k_auth: str = Cookie(None)):
        return self.verify_access_token(k_auth)
