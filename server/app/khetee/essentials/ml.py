import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import tensorflow as tf
import numpy as np


class Model:
    def __init__(self, mac):
        window_size = 10

        self.model = tf.keras.models.Sequential(
            [
                tf.keras.layers.Dense(
                    128, input_shape=[window_size], activation="relu"
                ),
                tf.keras.layers.Dense(64, activation="relu"),
                tf.keras.layers.Dense(1),
            ]
        )

        self.model.compile(
            loss="mse",
            optimizer=tf.keras.optimizers.SGD(learning_rate=1e-4, momentum=0.9),
        )

        self.model.load_weights("./model/" + mac + "/weights").expect_partial()

    def predict(self, value):
        prediction = self.model.predict(np.array(value)[np.newaxis])
        time = float(round(prediction[0][0], 1))
        in_seconds = time * 60
        minutes, seconds = int(in_seconds // 60), int(in_seconds % 60)

        if minutes < 10:
            minutes = "0" + str(minutes)
        else:
            minutes = str(minutes)

        if seconds < 10:
            seconds = "0" + str(seconds)
        else:
            seconds = str(seconds)

        return "00:" + minutes + ":" + seconds

    def summary(self):
        return self.model.summary()
