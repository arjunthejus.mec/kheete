function loginFunction() {
  var url = window.location.href

  data = JSON.stringify({
    "username": document.getElementById("email").value,
    "password": document.getElementById("password").value
  })

  var request = new XMLHttpRequest();
  request.open("POST", url.concat("/submit"), true);
  request.setRequestHeader("Content-Type", "application/json");
  request.onload = function(){
    jsonResponse = JSON.parse(request.response);
    if (jsonResponse.status_code == 401){
      document.getElementById("detail").innerHTML = "Invalid username or password";
    }
    else{
      console.log(request.response)
      window.location.href = "../profile"
    }
  }
  request.send(data)
}