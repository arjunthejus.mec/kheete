var url = window.location.href

function float2dollar(value){
    return ''+(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

function renderChart(data, labels) {
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'This week',
                data: data,
                borderColor: '#5fb759',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                
            }]
        },
        options: {            
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value, index, values) {
                            return float2dollar(value);
                        }
                    }
                }]                
            }
        },
    });
}
function processData(response){
    //find a better implementation for this function
    intermediateData = response.replaceAll("[",'').replaceAll("]", '');
    console.log(intermediateData)
    returnData = intermediateData.split(',')
    
    //the first half of the data is duration and the second half are labels
    var length = returnData.length
    return [returnData.slice(0, length/2), returnData.slice(length/2, length)]
}

$("#renderBtn").click(
    function () {
        var data = [];
        var labels = [];
        
        var request = new XMLHttpRequest();
        request.open("POST", url.concat("/graph"), true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function(){
            data = processData(request.response)
            renderChart(data[0], data[1]);
        }
        request.send()
    }
);