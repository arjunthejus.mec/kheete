var s, m, h, t;
var timer_is_on = 0;

function timedCount() {
    document.getElementById("secs").innerHTML = s;
    document.getElementById("mins").innerHTML = m;
    document.getElementById("hours").innerHTML = h;
    s = s - 1;

    if (h == 0 && m == 0 && s < 0) {
        stopCount();
    }

    if (s < 0) {
        m = m - 1;
        s = 59;
    }
    if (m < 0) {
        h = h - 1;
        m = 59;
    }
    if (timer_is_on == 1) {
        t = setTimeout(timedCount, 1000);
    }
    if (timer_is_on == 0) {
        stopCount();
    }
}

function startCount() {
    var time = document.getElementById('time').value
    time = time.split(":")
    s = parseInt(time[2]);
    m = parseInt(time[1]);
    h = parseInt(time[0]);
    if (!timer_is_on) {
        timer_is_on = 1;
        timedCount();
    }
}

function stopCount() {
    clearTimeout(t);
    timer_is_on = 0;
}
function start() {
    var url = window.location.origin
    var data = JSON.stringify({ "time": document.getElementById('time').value })

    var request = new XMLHttpRequest();
    request.open("POST", url.concat("/start"), true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function () {
        if (request.status == 200) {
            startCount();
        }
        else {
            console.log(request.status)
        }
    }
    request.send(data)
}
function stop() {
    var url = window.location.origin

    var request = new XMLHttpRequest();
    request.open("POST", url.concat("/stop"), true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function () {
        if (request.status == 200) {
            stopCount();
        }
        else {
            console.log(request.status)
        }
    }
    request.send()
}