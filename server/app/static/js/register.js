function registerFunction() {
    var url = window.location.href
    url = url.split('?')[0]
    
    data = JSON.stringify({
        "name": document.getElementById("name").value,
        "email": document.getElementById("email").value,
        "mac": document.getElementById("mac").value,
        "password1": document.getElementById("password1").value,
        "password2": document.getElementById("password2").value
    })
  
    var request = new XMLHttpRequest();
    request.open("POST", url.concat("/submit"), true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function(){
        if (request.status == 200){
            window.location.href ='../login'
        }
        else{
            jsonResponse = JSON.parse(request.response)
            document.getElementById("detail").innerHTML = jsonResponse.detail[0].msg;
        }

    }
    request.send(data)
  }