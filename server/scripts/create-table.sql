DROP TABLE IF EXISTS user_credentials, mac_list;

CREATE TABLE "user_credentials"(
    "user_id" integer UNIQUE GENERATED ALWAYS AS IDENTITY,
    "name" text NOT NULL,
    "username" text UNIQUE NOT NULL,
    "password" text NOT NULL
);

CREATE TABLE "mac_list"(
    "id" integer GENERATED ALWAYS AS IDENTITY,
    "mac" text UNIQUE NOT NULL,
    "user_id" integer,
    CONSTRAINT fk_id FOREIGN KEY(user_id) REFERENCES user_credentials(user_id) ON DELETE SET NULL
);